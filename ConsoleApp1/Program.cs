﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types.Enums;
using Microsoft.VisualBasic;

namespace TelegramBotExperiments
{

    class Program
    {
        static ITelegramBotClient bot = new TelegramBotClient("6959971963:AAFVxmdDyMIYP5W0GDxMkGrMC0iyiQSoGlE");
        public static async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            if (update.Type == Telegram.Bot.Types.Enums.UpdateType.Message)
            {
                var message = update.Message;
                if (message.Text == "/start")
                {
                    await botClient.SendTextMessageAsync(message.Chat, "Бот запущен");
                }

                switch (update.Message.Type)
                {
                    case MessageType.Text:
                        Console.WriteLine($"Новое сообщение от {update.Message.From.Username}: {update.Message.Text}");
                        break;
                    case MessageType.Sticker:
                        Console.WriteLine($"Новое сообщение от {update.Message.From.Username}: Стикер");
                        break;
                    case MessageType.Audio:
                        Console.WriteLine($"Новое сообщение от {update.Message.From.Username}: Аудио");
                        break;
                    case MessageType.Video:
                        Console.WriteLine($"Новое сообщение от {update.Message.From.Username}: Видео");
                        break;
                    case MessageType.Photo:
                        Console.WriteLine($"Новое сообщение от {update.Message.From.Username}: Фото");
                        break;
                    case MessageType.Poll:
                        Console.WriteLine($"Новое сообщение от {update.Message.From.Username}: Опрос");
                        break;
                    case MessageType.Document:
                        Console.WriteLine($"Новое сообщение от {update.Message.From.Username}: Документ");
                        break;
                    case MessageType.Voice:
                        Console.WriteLine($"Новое сообщение от {update.Message.From.Username}: Голосовое сообщение");
                        break;
                }
            }
        }

        public static async Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(exception));
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Бот запущен");
            var cts = new CancellationTokenSource();
            var cancellationToken = cts.Token;
            ReceiverOptions receiverOptions = new()
            {
                AllowedUpdates = Array.Empty<UpdateType>()
            };
            bot.StartReceiving(HandleUpdateAsync, HandleErrorAsync, receiverOptions, cancellationToken);
            Console.ReadLine();
        }
    }
}